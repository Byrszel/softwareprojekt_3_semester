package model;

import java.awt.image.BufferedImage;

import utils.Punkt;

public class Unit {

	//Attribute
	private Punkt position;
	private BufferedImage bild;
	private String farbe;
	
	//Getter und Setter
	public Punkt getPosition() {
		return position;
	}

	public void setPosition(Punkt position) {
		this.position = position;
	}

	public BufferedImage getBild() {
		return bild;
	}

	public void setBild(BufferedImage bild) {
		this.bild = bild;
	}

	public String getFarbe() {
		return farbe;
	}

	public void setFarbe(String farbe) {
		this.farbe = farbe;
	}
	
	//Zum Testen
	@Override
	public String toString() {
		return "Unit [position=" + position + ", bild=" + bild + ", farbe=" + farbe + "]";
	}
	
}
