package model;

import java.util.Random;

import logik.Controller;
import utils.Punkt;
import view.Main;

public class Spaceship extends Unit {

	public boolean fliegt;
	
	// Konstruktoren
	public Spaceship() {
		super();
	}

	public Spaceship(Punkt position, String farbe) {
		super();
		this.setPosition(position);
		this.setFarbe(farbe);
		this.fliegt = false;
		switch (this.getFarbe()) {
		case "braun":
			this.setBild(Main.einheitBlau);
			break;
		case "rot":
			this.setBild(Main.einheitRot);
			break;
		case "gruen":
			this.setBild(Main.einheitGruen);
			break;

		default:
			break;
		}
	}

	// Methoden

	/*
	 * Das Spaceship kollidiert mit einer gegnerischen Einheit.
	 */
	public void kollisionEinheit(Spaceship gegnerEinheit) {
		this.sterben();
		gegnerEinheit.sterben();
	}

	/*
	 * Das Spaceship kollidiert mit einer Sonne (z.B. einer gegnerischen Sonne).
	 */
	public void kollisionSonne() {
	}

	/*
	 * Das Spaceship bewegt sich zur angegebenden Position.
	 * 
	 * @param position - Punkt zu dem das Raumschiff fliegen soll
	 */
	public void flyToPosition(Punkt position) {
		Fliegen t1 = new Fliegen(this, position);
		t1.start();
		this.fliegt = true;
	}

	/*
	 * move-Logik
	 * 
	 * @param position - Punkt zu dem das Raumschiff fliegen soll
	 */
	public synchronized void move(Punkt position) {
		for (int xNow = this.getPosition().getX(), yNow = this.getPosition().getY(); xNow < position.getX() || yNow < position.getY() || xNow > position.getX() || yNow > position.getY();) {
			if (xNow < position.getX()) {
				xNow++;
			}
			if (xNow > position.getX()) {
				xNow--;
			}
			if (yNow < position.getY()) {
				yNow++;
			}
			if (yNow > position.getY()) {
				yNow--;
			}
			this.setPosition(new Punkt(xNow,yNow));
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		this.fliegt = false;
	}
	
	/*
	 * In einem kleinem Umkreis um die aktuelle Position sucht sich das Spaceship
	 * eine neue Position.
	 */
	public synchronized Punkt verteileAnPosition(Punkt position) {
		Random rand = new Random();
		return new Punkt(position.getX()+rand.nextInt(40),position.getY()+rand.nextInt(40));
	}

	/*
	 * Das Spaceship wird markiert, um z.B. mit anderen Einheiten zu einer Position
	 * zu fliegen.
	 */
	public void markieren() {
	}

	/*
	 * Das Schiff soll nicht mehr als ausgewaehlt gelten.
	 */
	public void demarkieren() {
	}

	/*
	 * Das Raumschiff wird zerst�rt.
	 */
	private void sterben() {
		Controller.einheiten.remove(Controller.einheiten.indexOf(this));
	}

}
