package model;

import utils.Punkt;

public class Fliegen extends Thread{
	
	//Attribute zur Zwischenspeicherung
	Spaceship ship;
	Punkt position;
	
	//Konstruktor
	Fliegen(Spaceship ship, Punkt position){
		this.ship = ship;
		this.position = position;
	}
	
	/*
	 * Die Kordinaten x und y des Raumschiffs werden solange erhoeht/verringert
	 * bis es die gewuenschte Position erreicht hat.
	 */
	@Override 
	public void run() {
		ship.move(position);
	}
}
