package model;

import java.awt.image.BufferedImage;

import logik.Controller;
import utils.Punkt;
import view.Main;

public class Sun {

	//Attribute
	private int sunNumber;
	private Punkt position;
	private int anzahlEinheitProduktion;
	private BufferedImage bild;
	private String farbe;
	
	//Konstruktoren
	public Sun() {
		super();
	}
	
	public Sun(Punkt position, String farbe) {
		super();
		this.sunNumber = Controller.sonnen.size();
		this.position = position;
		this.farbe = farbe;
		this.anzahlEinheitProduktion = 1;
		switch (this.getFarbe()) {
		case "rot":
			this.setBild(Main.sonneRot);
			break;
		case "braun":
			this.setBild(Main.sonneBraun);
			break;
		case "gruen":
			this.setBild(Main.sonneGruen);
			break;

		default:
			this.setBild(Main.sonneNeutral);
			break;
		}
	}
	
	//Getter und Setter
	public Punkt getPosition() {
		return position;
	}
	public void setPosition(Punkt position) {
		this.position = position;
	}
	public int getAnzahlEinheitProduktion() {
		return anzahlEinheitProduktion;
	}
	public void setAnzahlEinheitProduktion(int anzahlEinheitProduktion) {
		this.anzahlEinheitProduktion = anzahlEinheitProduktion;
	}
	public BufferedImage getBild() {
		return bild;
	}
	public void setBild(BufferedImage bild) {
		this.bild = bild;
	}
	public String getFarbe() {
		return farbe;
	}
	public void setFarbe(String farbe) {
		this.farbe = farbe;
	}
	
	public int getSunNumber() {
		return sunNumber;
	}

	public void setSunNumber(int sunNumber) {
		this.sunNumber = sunNumber;
	}

	//Methoden
	
	/*
	 * Je nach Upgrade-Stand der Sonne wird eine
	 * entsprechende Anzahl Einheiten produziert.
	 */
	public void einheitProduzieren() {
		for (int i = 0; i < this.getAnzahlEinheitProduktion(); i++) {
			Spaceship ship = new Spaceship(new Punkt(this.getPosition().getX()+74,this.getPosition().getY()+74),this.getFarbe());
			Punkt p1 = Controller.einheitStartPunkt(ship, Controller.sonnen.get(this.getSunNumber()));
			ship.flyToPosition(p1);
			Controller.einheiten.add(ship);
		}
	}
	
	/*
	 * Die Sonne wird verbessert, 
	 * sodass sie mehr Einheiten produziert.
	 */
	public void upgrade() {}
	
	/*
	 * Die Sonne wird neutral.
	 */
	public void sterben() {}
	
	/*
	 * Die Sonne wird von einer AI 
	 * oder einem Spieler eingenommen.
	 */
	public void einnahme() {}
	
	/*
	 * Der Besitzer der Sonne wird zu 
	 * einer anderen Fraktion ge�ndert.
	 */
	public void besitzerWechseln(String colorNewOwner) {}
	
	//Zum Testen
	@Override
	public String toString() {
		return "Sun [sunNumber=" + sunNumber + ", position=" + position + ", anzahlEinheitProduktion="
				+ anzahlEinheitProduktion + ", bild=" + bild + ", farbe=" + farbe + "]";
	}
	
}
