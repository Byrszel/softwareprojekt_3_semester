package model;

import logik.Controller;
import utils.Punkt;

public class Kollision extends Thread {
	
	

	/*
	 * Dieser Algorithmus ist nicht der Effektivste!
	 */
	@Override
	public void run() {
		while (true) {
			Punkt shipA;
			Punkt shipB;
			for (int i = 0; i < Controller.einheiten.size(); i++) {
				shipA = Controller.einheiten.get(i).getPosition();
				for (int j = 1; j < Controller.einheiten.size(); j++) {
					if (j != i) {
						shipB = Controller.einheiten.get(j).getPosition();
						double x = shipB.getX() - shipA.getX();
						double y = shipB.getY() - shipA.getY();
						double distanz = Math.sqrt((x * x) + (y * y));
						if (Controller.einheiten.get(i).fliegt == false && distanz <= 40) {
							if (!Controller.einheiten.get(i).getFarbe().equals(Controller.einheiten.get(j).getFarbe())) {
							Controller.einheiten.get(i).flyToPosition(new Punkt(Controller.einheiten.get(j).getPosition().getX(), Controller.einheiten.get(j).getPosition().getY()));
							Controller.einheiten.get(j).flyToPosition(new Punkt(Controller.einheiten.get(i).getPosition().getX(), Controller.einheiten.get(i).getPosition().getY()));
							}
						}
						if (distanz <= 10) {
							if (!Controller.einheiten.get(i).getFarbe().equals(Controller.einheiten.get(j).getFarbe())) {
								Controller.einheiten.get(i).kollisionEinheit(Controller.einheiten.get(j));
								i = 0;
								j = 1;
							}
						}
					}
				}
			}
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
