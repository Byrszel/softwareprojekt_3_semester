package model;

public class AI {

	//Attribute
	private String name;
	private String farbe;
	
	//Getter und Setter
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFarbe() {
		return farbe;
	}

	public void setFarbe(String farbe) {
		this.farbe = farbe;
	}

	@Override
	public String toString() {
		return "AI [name=" + name + ", farbe=" + farbe + "]";
	}
		
}
