package view;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.image.BufferStrategy;

import javax.swing.JFrame;

import model.Kollision;
import utils.Input;

public class GUI extends JFrame{

	public static GameScreen gameScreen;
	public static Kollision kollision;
    static BufferStrategy buffer;
	Input input;
    
	public void start() {
		kollision.start();
		gameScreen.start();
	}
	
	/*
	 * Konstruktor der Spiel-GUI
	 */
	public GUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(250, 100, 1300, 900);
		setResizable(false);
		gameScreen = new GameScreen();
		gameScreen.setSize(new Dimension(1300,900));
		gameScreen.setIgnoreRepaint(true);
		gameScreen.setVisible(true);
		input = new Input();
		addMouseListener(input);
		add(gameScreen);
		gameScreen.addMouseListener(input);
		gameScreen.addMouseMotionListener(input);
		kollision = new Kollision();
		setVisible(true);
	}

	/*
	 * Erstellt eine BufferStrategy, 
	 * damit mit Hilfe eines 2-Frame-Buffers gezeichnet werden kann.
	 */
	public void erstelleBuffer() {
		gameScreen.createBufferStrategy(2);
	    buffer = gameScreen.getBufferStrategy();
	}

}
