package view;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import logik.Controller;
import model.Sun;
import utils.Punkt;

public class Main {

	//Sonnen-Bilder
	public static BufferedImage sonneRot;
	public static BufferedImage sonneBraun;
	public static BufferedImage sonneGruen;
	public static BufferedImage sonneNeutral;
	//Einheiten-Bilder
	public static BufferedImage einheitRot;
	public static BufferedImage einheitBlau;
	public static BufferedImage einheitGruen;
	//Spiel-Logo
	public static BufferedImage logo;

	/*
	 * Daten, welche das Programm benoetigt, erstellen.
	 */
	public void init() {
		//Bilder einlesen
		try {
			sonneRot = ImageIO.read(getClass().getResource("/bilder/sonne_doge_150_150.png"));
			sonneBraun = ImageIO.read(getClass().getResource("/bilder/sonne_nogger_150_150.png"));
			sonneGruen = ImageIO.read(getClass().getResource("/bilder/sonne_pepe_150_150.png"));
			sonneNeutral = ImageIO.read(getClass().getResource("/bilder/sonne_durchsichtig_150_150.png"));
			einheitRot = ImageIO.read(getClass().getResource("/bilder/rotEinheit.png"));
			einheitBlau = ImageIO.read(getClass().getResource("/bilder/braunEinheit.png"));
			einheitGruen = ImageIO.read(getClass().getResource("/bilder/gruenEinheit.png"));
			logo = ImageIO.read(getClass().getResource("/bilder/Conquest.png"));;
		} catch (IOException e) {
			e.printStackTrace();
		}
		//Sonnen erstellen
		Sun sonne1 = new Sun(new Punkt(100, 50), "rot");
		Controller.sonnen.add(sonne1);
		Sun sonne2 = new Sun(new Punkt(1000, 50), "braun");
		Controller.sonnen.add(sonne2);
		Sun sonne3 = new Sun(new Punkt(550, 620), "gruen");
		Controller.sonnen.add(sonne3);
		Sun sonne4 = new Sun(new Punkt(550, 300), "");
		Controller.sonnen.add(sonne4);
	}

}
