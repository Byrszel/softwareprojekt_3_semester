package view;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import model.Kollision;
import model.Spieler;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Spielstart extends JFrame {

	private JPanel contentPane;
	private String[] farben = {"rot", "braun", "gruen"};
	public static Spieler spieler;

	/*
	 * Main-Methode, zum starten des Programms
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main main = new Main();
					main.init();
					Spielstart frame = new Spielstart();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/*
	 * Konstruktor der Spielstart-GUI
	 */
	public Spielstart() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(700, 400, 600, 450);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.setBackground(Color.DARK_GRAY);;
		
		JComboBox comboBox = new JComboBox(this.farben);
		comboBox.setBounds(220, 100, 160, 31);
		contentPane.add(comboBox);
		JLabel labelBild = new JLabel();
		labelBild.setBounds(400,20,150,150);
		
		ImageIcon icon = new ImageIcon(Main.logo);
		labelBild.setIcon(icon);
		JButton btnNewButton = new JButton();
		btnNewButton.setForeground(Color.WHITE);
		btnNewButton.setText("Spiel starten");
		btnNewButton.setFocusable(false);
		btnNewButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				GUI frame = new GUI();
				frame.erstelleBuffer();
				spieler = new Spieler();
				spieler.setFarbe((String) comboBox.getSelectedItem());
				setVisible(false);
				frame.start();
			}
		});
		btnNewButton.setBackground(Color.gray);
		btnNewButton.setVisible(true);
		btnNewButton.setBounds(220,300,150,70);
		contentPane.add(btnNewButton);
		contentPane.add(labelBild);
		setVisible(true);
	}
	
	

}
