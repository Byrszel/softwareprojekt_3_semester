package view;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.util.ArrayList;
import java.util.Random;

import logik.Controller;
import utils.FPS;
import utils.Input;
import utils.Punkt;

public class GameScreen extends Canvas implements Runnable{

	private int hilfZahlFuerSterne = 0;
	private ArrayList<Punkt> sterne = new ArrayList<Punkt>();
	
	/*
	 * Zeichnet die aktuellesten Daten
	 */
	public void render(long lastFPSupdate) {

		//Graphics-Objekte zum Zeichen
		Graphics graphics =  GUI.buffer.getDrawGraphics();
		Graphics2D graphics2D = (Graphics2D) graphics;
				
		//Kantenglaettung aktivieren
		graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		
		//Hintergrund
		graphics2D.setColor(Color.DARK_GRAY);
		graphics2D.fillRect(0, 0, GUI.gameScreen.getWidth(), GUI.gameScreen.getHeight());
		
		//Sterne
		if (hilfZahlFuerSterne == 0) {
			Random rand = new Random();
			for (int i = 0; i < 28; i++) {
				int x = rand.nextInt(1200)+50;
				int y = rand.nextInt(800)+50; 
				Punkt stern = new Punkt(x,y);
				sterne.add(stern);
			}
			hilfZahlFuerSterne++;
		}
		graphics2D.setColor(Color.white);
		graphics2D.setFont(new Font("Courier New", Font.PLAIN, 26));
		for (int i = 0; i < sterne.size(); i++) {
			graphics2D.drawString(String.format( "."), sterne.get(i).getX(), sterne.get(i).getY());
		}
		
		//Auswahl-Rechteck
		graphics2D.setColor(Color.red);
		if (Input.pressed == true) {
			graphics2D.drawRect(Input.oldX, Input.oldY, Math.abs(Input.currentX-Input.oldX), Math.abs(Input.currentY-Input.oldY));
		}
		
		//Sonnen zeichnen
		for (int i = 0; i < Controller.sonnen.size(); i++) {
			graphics2D.drawImage(Controller.sonnen.get(i).getBild(), Controller.sonnen.get(i).getPosition().getX(),
					Controller.sonnen.get(i).getPosition().getY(), null);
		}
		//Einheiten zeichnen
		for (int i = 0; i < Controller.einheiten.size(); i++) {
			graphics2D.drawImage(Controller.einheiten.get(i).getBild(),
					Controller.einheiten.get(i).getPosition().getX(),
					Controller.einheiten.get(i).getPosition().getY(), null);
		}

	    //FPS anzeigen
		graphics2D.setColor(Color.white);
		graphics2D.setFont(new Font("Courier New", Font.PLAIN, 16));
		graphics2D.drawString(String.format( "FPS: %s", FPS.fpsBackup), 20, 20);
		if (lastFPSupdate >= 1000000000)
			FPS.fpsBackup = FPS.fps;

		//Buffer aufrufen
	    if(!GUI.buffer.contentsLost())
	        GUI.buffer.show();
	    
	    //Alt-Daten entfernen
	    if (graphics != null) {
			graphics.dispose();
		}
	    if (graphics2D != null) {
			graphics2D.dispose();
		}
	}
	
	/*
	 * Berechnet die FPS und sorgt fuer den "Spielfluss"
	 */
	public void gameloop() {
		long lastLooptime = System.nanoTime();
		long lastFPSupdate = 0;

		while (true) {
			long now = System.nanoTime();
			long updateCounter = now - lastLooptime;
			lastLooptime = now;
			lastFPSupdate += updateCounter;
				FPS.fps++;

				this.render(lastFPSupdate);

				if (lastFPSupdate >= 1000000000) {
					FPS.fps = 0;
					lastFPSupdate = 0;
				}
				
				if (GUI.kollision.isInterrupted() == true) {
					GUI.kollision.start();
				}
				if (FPS.fps == 0 ) {
					for (int i = 0; i < Controller.sonnen.size(); i++) {
						if (Controller.sonnen.get(i).getFarbe() != "") {
							Controller.sonnen.get(i).einheitProduzieren();
						}
					}
				}
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

	}

	public void start() {
		Thread gameScreenThread = new Thread(this);
		gameScreenThread.start();
	}
	
	@Override
	public void run() {
		this.gameloop();
	}
}
