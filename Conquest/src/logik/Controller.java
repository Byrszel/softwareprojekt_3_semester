package logik;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import model.Spaceship;
import model.Sun;
import utils.Punkt;
import view.Spielstart;

public class Controller {

	//Listen in dennen alle Sonnen und Einheiten gespeichert sind
	public static ArrayList<Sun> sonnen = new ArrayList<Sun>();
	public static ArrayList<Spaceship> einheiten = new ArrayList<Spaceship>();
	public static ArrayList<Spaceship> selected = new ArrayList<Spaceship>();
	
	/*
	 * Die Methode sorgt dafuer, dass die Startposition, die kurz nach der Erzeugung einer Einheit
	 * durch eine Sonne benoetigt wird, berechnet wird.
	 * @param einheit  - Das Raumschiff fuer das die Position errechnet werden soll
	 * @param sonne    - Die Sonne, welche die neue Einheit erzeugt hat
	 * @return Punkt   - Die Positon, bei der die Einheit starten soll
	 */
	public static Punkt einheitStartPunkt(Spaceship einheit, Sun sonne) {
		int xSunMidPosition = sonne.getPosition().getX()+73;
		int ySunMidPosition = sonne.getPosition().getY()+73;
        Random rand = new Random();
        int distance = rand.nextInt(15) + 100;
        double angle = Math.toRadians((double) rand.nextInt(360));
        int x = (int) (Math.cos(angle) * distance + xSunMidPosition);
        x = Math.max(x, 0);
        int y = (int) (Math.sin(angle) * distance + ySunMidPosition);
        y = Math.max(y, 0);
		return new Punkt(x,y);
	}
	
	/*
	 * Waehlt einheiten aus.
	 * @return List<Spaceship>  - Liste der ausgewaehlten Einheiten
	 */
	public static void einheitAuswaehlen(int startX, int startY, int endX, int endY){
		for (int i = 0; i < Controller.einheiten.size(); i++) {
			if (Controller.einheiten.get(i).getPosition().getX() >= startX && Controller.einheiten.get(i).getPosition().getX() <= endX && Controller.einheiten.get(i).getPosition().getY() >= startY && Controller.einheiten.get(i).getPosition().getY() <= endY) {
				if (Spielstart.spieler.getFarbe().equals(Controller.einheiten.get(i).getFarbe())) {
					selected.add(Controller.einheiten.get(i));
				}
			}
		}
	}
		
}
