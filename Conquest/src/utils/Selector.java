package utils;

import logik.Controller;

public class Selector extends Thread{

	int x,y;
	
	public Selector(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	@Override 
	public void run() {
		for (int i = 0; i < Controller.selected.size(); i++) {
			Controller.selected.get(i).flyToPosition(Controller.selected.get(i).verteileAnPosition(new Punkt(this.x, this.y)));
		}
	}
	
}
