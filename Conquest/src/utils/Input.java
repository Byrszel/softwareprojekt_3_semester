package utils;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import logik.Controller;

public class Input implements MouseListener, MouseMotionListener{

    public static int currentX, currentY, oldX, oldY = 0;
    public static boolean pressed = false;
	int zwischenZaehler = 0;
    
    @Override
    public void mousePressed(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {
        Controller.einheitAuswaehlen(oldX, oldY, currentX, currentY);
        zwischenZaehler = 0;
    }

	@Override
	public void mouseClicked(MouseEvent arg0) {
		Selector selector = new Selector(arg0.getX(),arg0.getY());
		selector.start();
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		
	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		if (zwischenZaehler == 0) {
			oldX = arg0.getX();
			oldY = arg0.getY();
		}
		zwischenZaehler++;
		currentX = arg0.getX();
		currentY = arg0.getY();
		pressed = true;
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		Controller.selected.clear();
		pressed = false;
	}

}
