package utils;

public class FPS {
    public static int fps = 0;
    public static int fpsBackup = 0;

    public int getFps()
    {
        return fps;
    }

    public void setFps(int fps)
    {
        this.fps = fps;
    }
}
